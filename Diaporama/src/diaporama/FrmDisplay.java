/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * FrmDisplay.java
 *
 * Created on 14 oct. 2014, 16:39:14
 */
package diaporama;

import fileChooser.ImageFiltreTypeAll;
import fileChooser.ImageFiltreTypeGif;
import fileChooser.ImageFileView;
import fileChooser.ImagePreview;
import fileChooser.ImageFiltreTypeJpg;
import fileChooser.ImageFiltreTypePng;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import listItem.ItemCellRender;
import listItem.ListItem;
import pnlImage.PnlImage;

/**
 *
 * @author le_forro
 */
public class FrmDisplay extends javax.swing.JFrame 
{
    private PnlImage image;
    private DefaultListModel <ListItem> imageListe;
    private JFileChooser fileChooser;
    private Timer time;
    private boolean isPaused = true;
    private ActionListener timerListener;
    private Random rand;
    private ArrayList <ImageIcon> playState;
            
    /** Creates new form FrmDisplay */
    public FrmDisplay()
    {
        this.initComponents();
        this.initImageListe();
        this.initFileChooser();
        this.initTimer();
        this.initPlayStateIcon();
        this.rand = new Random();
        /*
        this.imageListe.addElement(new ListItem("C:\\Users\\romain\\Pictures\\SP_A0037.jpg"));
        this.imageListe.addElement(new ListItem("C:\\Users\\romain\\Pictures\\ToLoveRu.jpg"));
        this.imageListe.addElement(new ListItem("C:\\Users\\romain\\Pictures\\Rayman_Raving_Rabbids_3.jpg"));
        */
    }
    
    private void initTimer()
    {
        this.timerListener = new ActionListener() 
                                {

                                    @Override
                                    public void actionPerformed(ActionEvent e) 
                                    {
                                        if(!isPaused)
                                        {
                                            int newVal = 10 + pbTimeProgress.getValue();
                                            pbTimeProgress.setValue(10 + pbTimeProgress.getValue());
                                            if(pbTimeProgress.getMaximum() <= newVal)
                                            {
                                                pbTimeProgress.setValue(0);
                                                changeDisplayedImage(3);
                                            }
                                            //pour provoque un fade out
                                            if(pbTimeProgress.getMaximum() <= (newVal + 2))
                                                if(!image.isFadding())
                                                {
                                                    image.setState(0);
                                                    image.setFadding(true);
                                                }
                                        }
                                    }
                                };
        this.time = new Timer(100, this.timerListener);
    }
    
    private void initImageListe()
    {
        this.imageListe = new DefaultListModel<>();
        this.lstImage.setModel(this.imageListe);
        this.lstImage.setCellRenderer(new ItemCellRender());
    }
    
    private void initFileChooser()
    {
        this.fileChooser = new JFileChooser();
        
        //on met le filtre de fichier par defaut
        this.fileChooser.setFileFilter(new ImageFiltreTypeAll());
        
        //on ajoute d'autres filtres pour les extensions de fichier
        this.fileChooser.addChoosableFileFilter(new ImageFiltreTypePng());
        this.fileChooser.addChoosableFileFilter(new ImageFiltreTypeJpg());
        this.fileChooser.addChoosableFileFilter(new ImageFiltreTypeGif());
        
        //on desactive le filtre touts dans le choix de fichier
        this.fileChooser.setAcceptAllFileFilterUsed(false);
        
        //on met des icones personaliser pour les fichier
        this.fileChooser.setFileView(new ImageFileView());
        
        //on ajout un panneau pour previsualiser l'image selectionner
        this.fileChooser.setAccessory(new ImagePreview(this.fileChooser));
        
        //on autorise la selection de plusieur fichier
        this.fileChooser.setMultiSelectionEnabled(true);
    }
    
    private void initPlayStateIcon()
    {
        java.net.URL imgURL0 = this.getClass().getResource("/diaporama/icon/play.png");
        java.net.URL imgURL1 = this.getClass().getResource("/diaporama/icon/pause.png");
        java.net.URL imgURL2 = this.getClass().getResource("/diaporama/icon/stop.png");
        ArrayList <ImageIcon> lst = new ArrayList <>();
        lst.add(new ImageIcon(imgURL0));
        lst.add(new ImageIcon(imgURL1));
        lst.add(new ImageIcon(imgURL2));
        this.playState = lst;
    }
    
    protected void changePlayState(String state)
    {
        String str = "l'état courant est "+state;
        switch(state)
        {
            case "play" :
                    this.lblTimeState.setIcon(this.playState.get(0));
                    this.lblTimeState.setToolTipText(str);
                break;
            case "pause":
                    this.lblTimeState.setIcon(this.playState.get(1));
                    this.lblTimeState.setToolTipText(str);
                break;
            case "stop" :
                    this.lblTimeState.setIcon(this.playState.get(2));
                    this.lblTimeState.setToolTipText(str);
                break;
            default: //do nothing
                break;
        }
    }
    
    /**
     * change the displayed image regarding param action
     * action list:
     *  1 - first
     *  2 - previous
     *  3 - next
     *  4 - last
     * @param action int value for action to change
     */
    public void changeDisplayedImage(int action)
    {
        System.out.println("[+] displayed image  number is "+action);
        //si la liste des image contien une image
        if(this.imageListe.size() > 0)
        {
            //on remet la barre de progression du timer a 0
            this.pbTimeProgress.setValue(0);
            if(this.lstImage.isSelectionEmpty())
            {
                if(action == 4)
                    this.setSelectedDiapo(this.imageListe.getSize() - 1);
                else
                    this.setSelectedDiapo(0);
                return;
            }
            
            int currentIndex = this.lstImage.getSelectedIndex();
            int nextIndex = currentIndex;
            int lastIndex = this.imageListe.getSize() - 1;
            switch(action)
            {
                case 1 :
                    nextIndex = 0;
                    break;
                case 2 :
                    if(currentIndex > 0)
                        nextIndex = currentIndex - 1;
                    else
                        if(this.tgBtnLoop.isSelected())
                            nextIndex = lastIndex;
                    break;
                case 3 :
                    if(this.tgBtnRandom.isSelected())
                    {
                        nextIndex = this.rand.nextInt(this.imageListe.getSize());
                    }
                    else
                    {
                        if(currentIndex < lastIndex)
                            nextIndex = currentIndex + 1;
                        else
                            if(this.tgBtnLoop.isSelected())
                                nextIndex = 0;
                            else
                                if(this.time.isRunning())
                                {
                                    this.time.stop();
                                    this.pbTimeProgress.setValue(0);
                                    this.changePlayState("stop");
                                }
                    }
                    break;
                case 4 :
                    nextIndex = lastIndex;
                    break;
                default:
                    //ignore change, no action;
                    break;
            }
            this.setSelectedDiapo(nextIndex);
        }
    }
    
    /**
     * set the selected index in diapo list
     * @param diapoIndex int value
     */
    public void setSelectedDiapo(int diapoIndex)
    {
        this.lstImage.ensureIndexIsVisible(diapoIndex);
        this.lstImage.setSelectedIndex(diapoIndex);
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        pnlFileDisp = new javax.swing.JPanel();
        scrlListeImage = new javax.swing.JScrollPane();
        lstImage = new javax.swing.JList();
        pnlNavigation = new javax.swing.JPanel();
        pnlImage = new javax.swing.JPanel();
        lblForFiltre = new javax.swing.JLabel();
        combFiltre = new javax.swing.JComboBox();
        btnSupprDiapo = new javax.swing.JButton();
        btnUpDiapo = new javax.swing.JButton();
        btnDownDiapo = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        combTransition = new javax.swing.JComboBox();
        pnlFile = new javax.swing.JPanel();
        btnOpen = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        pnlDiapositive = new javax.swing.JPanel();
        pnlDiapoNav = new javax.swing.JPanel();
        btnStart = new javax.swing.JButton();
        btnRetStep = new javax.swing.JButton();
        btnPlay = new javax.swing.JButton();
        btnPause = new javax.swing.JButton();
        btnStop = new javax.swing.JButton();
        btnNext = new javax.swing.JButton();
        btnEnd = new javax.swing.JButton();
        tgBtnRandom = new javax.swing.JToggleButton();
        tgBtnLoop = new javax.swing.JToggleButton();
        pnlDiapoSpeed = new javax.swing.JPanel();
        lblForVitesse = new javax.swing.JLabel();
        lblVitesse = new javax.swing.JLabel();
        sldVitesse = new javax.swing.JSlider();
        pbTimeProgress = new javax.swing.JProgressBar();
        lblForProgess = new javax.swing.JLabel();
        lblTimeState = new javax.swing.JLabel();
        pnlForImage = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(870, 880));

        pnlFileDisp.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlFileDisp.setMaximumSize(new java.awt.Dimension(200, 300000));
        pnlFileDisp.setMinimumSize(new java.awt.Dimension(200, 800));
        pnlFileDisp.setPreferredSize(new java.awt.Dimension(200, 800));

        lstImage.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstImage.setToolTipText("");
        lstImage.setDragEnabled(true);
        lstImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lstImageMousePressed(evt);
            }
        });
        lstImage.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstImageValueChanged(evt);
            }
        });
        scrlListeImage.setViewportView(lstImage);

        javax.swing.GroupLayout pnlFileDispLayout = new javax.swing.GroupLayout(pnlFileDisp);
        pnlFileDisp.setLayout(pnlFileDispLayout);
        pnlFileDispLayout.setHorizontalGroup(
            pnlFileDispLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrlListeImage, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
        );
        pnlFileDispLayout.setVerticalGroup(
            pnlFileDispLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrlListeImage, javax.swing.GroupLayout.DEFAULT_SIZE, 921, Short.MAX_VALUE)
        );

        pnlNavigation.setMaximumSize(new java.awt.Dimension(300000, 100));
        pnlNavigation.setMinimumSize(new java.awt.Dimension(590, 100));

        pnlImage.setBorder(javax.swing.BorderFactory.createTitledBorder("Image"));

        lblForFiltre.setText("Filtre:");

        combFiltre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normale", "Noir et Blanc", "Sepia", "Negatif" }));
        combFiltre.setToolTipText("Selection du filtre a appliquer a l'image");
        combFiltre.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combFiltreItemStateChanged(evt);
            }
        });

        btnSupprDiapo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/supprimer.png"))); // NOI18N
        btnSupprDiapo.setText("Enlever");
        btnSupprDiapo.setToolTipText("Enlever de la diapo de la liste");
        btnSupprDiapo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnSupprDiapoMousePressed(evt);
            }
        });

        btnUpDiapo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/up.png"))); // NOI18N
        btnUpDiapo.setText("Monter");
        btnUpDiapo.setToolTipText("Monter la diapo dans la liste");
        btnUpDiapo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnUpDiapoMousePressed(evt);
            }
        });

        btnDownDiapo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/down.png"))); // NOI18N
        btnDownDiapo.setText("Descendre");
        btnDownDiapo.setToolTipText("Descendre la diapo dans la liste");
        btnDownDiapo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnDownDiapoMousePressed(evt);
            }
        });

        jLabel1.setText("Transition");

        combTransition.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Acune", "Fadding", "Blanc" }));
        combTransition.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combTransitionItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout pnlImageLayout = new javax.swing.GroupLayout(pnlImage);
        pnlImage.setLayout(pnlImageLayout);
        pnlImageLayout.setHorizontalGroup(
            pnlImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlImageLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlImageLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(combTransition, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlImageLayout.createSequentialGroup()
                        .addComponent(lblForFiltre)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(combFiltre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnSupprDiapo)
                    .addComponent(btnUpDiapo)
                    .addComponent(btnDownDiapo))
                .addContainerGap(11, Short.MAX_VALUE))
        );
        pnlImageLayout.setVerticalGroup(
            pnlImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlImageLayout.createSequentialGroup()
                .addGroup(pnlImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(combFiltre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblForFiltre))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(combTransition, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSupprDiapo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnUpDiapo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDownDiapo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlFile.setBorder(javax.swing.BorderFactory.createTitledBorder("Fichier"));

        btnOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/open.png"))); // NOI18N
        btnOpen.setText("Ouvrir");
        btnOpen.setToolTipText("Ouvrir/Ajouter une image");
        btnOpen.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnOpenMousePressed(evt);
            }
        });

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/save.png"))); // NOI18N
        btnSave.setText("Enregistrer");
        btnSave.setToolTipText("Enregistrer l'image afficher");
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnSaveMousePressed(evt);
            }
        });

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/delete.png"))); // NOI18N
        btnDelete.setText("Supprimer");
        btnDelete.setToolTipText("Supprimer le fichier de l'image selectionner");
        btnDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnDeleteMousePressed(evt);
            }
        });

        javax.swing.GroupLayout pnlFileLayout = new javax.swing.GroupLayout(pnlFile);
        pnlFile.setLayout(pnlFileLayout);
        pnlFileLayout.setHorizontalGroup(
            pnlFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFileLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnOpen)
                    .addComponent(btnSave)
                    .addComponent(btnDelete))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlFileLayout.setVerticalGroup(
            pnlFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFileLayout.createSequentialGroup()
                .addComponent(btnOpen)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDelete)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlDiapositive.setBorder(javax.swing.BorderFactory.createTitledBorder("Diapositive"));

        pnlDiapoNav.setBorder(javax.swing.BorderFactory.createTitledBorder("Navigation"));
        pnlDiapoNav.setLayout(new java.awt.GridBagLayout());

        btnStart.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/fastBackward.png"))); // NOI18N
        btnStart.setToolTipText("debut");
        btnStart.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnStartMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = -26;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(16, 16, 29, 0);
        pnlDiapoNav.add(btnStart, gridBagConstraints);

        btnRetStep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/previous.png"))); // NOI18N
        btnRetStep.setToolTipText("Precedent");
        btnRetStep.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnRetStepMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = -26;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(16, 6, 29, 0);
        pnlDiapoNav.add(btnRetStep, gridBagConstraints);

        btnPlay.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/play.png"))); // NOI18N
        btnPlay.setToolTipText("Lecture");
        btnPlay.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnPlayMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = -22;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(16, 6, 29, 0);
        pnlDiapoNav.add(btnPlay, gridBagConstraints);

        btnPause.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/pause.png"))); // NOI18N
        btnPause.setToolTipText("Pause");
        btnPause.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnPauseMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = -25;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(16, 6, 29, 0);
        pnlDiapoNav.add(btnPause, gridBagConstraints);

        btnStop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/stop.png"))); // NOI18N
        btnStop.setToolTipText("Stop");
        btnStop.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnStopMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = -25;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(16, 6, 29, 0);
        pnlDiapoNav.add(btnStop, gridBagConstraints);

        btnNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/next.png"))); // NOI18N
        btnNext.setToolTipText("Suivant");
        btnNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnNextMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = -25;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(16, 6, 29, 0);
        pnlDiapoNav.add(btnNext, gridBagConstraints);

        btnEnd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/fastForward.png"))); // NOI18N
        btnEnd.setToolTipText("Fin");
        btnEnd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnEndMousePressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = -25;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(16, 6, 29, 0);
        pnlDiapoNav.add(btnEnd, gridBagConstraints);

        tgBtnRandom.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/shuffle.png"))); // NOI18N
        tgBtnRandom.setToolTipText("Aleatoire");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = -23;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(16, 6, 29, 0);
        pnlDiapoNav.add(tgBtnRandom, gridBagConstraints);

        tgBtnLoop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/loop.png"))); // NOI18N
        tgBtnLoop.setToolTipText("Boucle/Repetition");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = -27;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(16, 4, 0, 18);
        pnlDiapoNav.add(tgBtnLoop, gridBagConstraints);

        pnlDiapoSpeed.setBorder(javax.swing.BorderFactory.createTitledBorder("Defilement"));

        lblForVitesse.setText("Vitesse:");

        lblVitesse.setText("15s");

        sldVitesse.setMaximum(120);
        sldVitesse.setMinimum(5);
        sldVitesse.setToolTipText("Selection du temps d'affichage d'une diapositive");
        sldVitesse.setValue(15);
        sldVitesse.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sldVitesseStateChanged(evt);
            }
        });

        pbTimeProgress.setMaximum(1500);
        pbTimeProgress.setToolTipText("temps avant le prochain changement");

        lblForProgess.setText("Temps ecouler:");
        lblForProgess.setToolTipText("");

        lblTimeState.setIcon(new javax.swing.ImageIcon(getClass().getResource("/diaporama/icon/stop.png"))); // NOI18N
        lblTimeState.setToolTipText("currentState");

        javax.swing.GroupLayout pnlDiapoSpeedLayout = new javax.swing.GroupLayout(pnlDiapoSpeed);
        pnlDiapoSpeed.setLayout(pnlDiapoSpeedLayout);
        pnlDiapoSpeedLayout.setHorizontalGroup(
            pnlDiapoSpeedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDiapoSpeedLayout.createSequentialGroup()
                .addGroup(pnlDiapoSpeedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDiapoSpeedLayout.createSequentialGroup()
                        .addComponent(lblForVitesse)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblVitesse))
                    .addComponent(lblForProgess))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDiapoSpeedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDiapoSpeedLayout.createSequentialGroup()
                        .addComponent(lblTimeState)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pbTimeProgress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(sldVitesse, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        pnlDiapoSpeedLayout.setVerticalGroup(
            pnlDiapoSpeedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDiapoSpeedLayout.createSequentialGroup()
                .addGroup(pnlDiapoSpeedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(sldVitesse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlDiapoSpeedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblForVitesse)
                        .addComponent(lblVitesse)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlDiapoSpeedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pbTimeProgress, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblForProgess, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblTimeState, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        javax.swing.GroupLayout pnlDiapositiveLayout = new javax.swing.GroupLayout(pnlDiapositive);
        pnlDiapositive.setLayout(pnlDiapositiveLayout);
        pnlDiapositiveLayout.setHorizontalGroup(
            pnlDiapositiveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDiapositiveLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDiapositiveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlDiapoSpeed, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlDiapoNav, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        pnlDiapositiveLayout.setVerticalGroup(
            pnlDiapositiveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDiapositiveLayout.createSequentialGroup()
                .addComponent(pnlDiapoNav, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlDiapoSpeed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout pnlNavigationLayout = new javax.swing.GroupLayout(pnlNavigation);
        pnlNavigation.setLayout(pnlNavigationLayout);
        pnlNavigationLayout.setHorizontalGroup(
            pnlNavigationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlNavigationLayout.createSequentialGroup()
                .addComponent(pnlFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlDiapositive, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pnlNavigationLayout.setVerticalGroup(
            pnlNavigationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlNavigationLayout.createSequentialGroup()
                .addGroup(pnlNavigationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pnlImage, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlFile, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlDiapositive, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pnlForImage.setMinimumSize(new java.awt.Dimension(590, 690));
        pnlForImage.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                pnlForImageComponentResized(evt);
            }
        });
        pnlForImage.setLayout(new java.awt.GridLayout(1, 0));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlFileDisp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pnlNavigation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlForImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlFileDisp, javax.swing.GroupLayout.DEFAULT_SIZE, 925, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlForImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnlNavigation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void sldVitesseStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sldVitesseStateChanged
// TODO add your handling code here:
    int speedValue = this.sldVitesse.getValue();
    int maximun = speedValue * 100;
    this.lblVitesse.setText((speedValue < 10 ? "0" : "")+Integer.toString(speedValue) +"s");
    this.pbTimeProgress.setMaximum(maximun);
    if(this.pbTimeProgress.getValue() > maximun)
        this.pbTimeProgress.setValue(maximun);
}//GEN-LAST:event_sldVitesseStateChanged

private void btnOpenMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnOpenMousePressed
    
    this.fileChooser.setMultiSelectionEnabled(true);
    int returnVal = this.fileChooser.showDialog(this, "Selectionner une/des image(s)");
    //Process the results.
     if (returnVal == JFileChooser.APPROVE_OPTION)
     {
            File[] files = this.fileChooser.getSelectedFiles();
            for(File file: files)
            {
                System.out.println("Opening file: " + file.getName()+ "\n"+
                                    "File Path" + file.getAbsolutePath() +".");
                this.imageListe.addElement(new ListItem(file.getAbsolutePath()));
            }
            this.setSelectedDiapo(this.imageListe.getSize()-1);
            File currentDirectory = this.fileChooser.getCurrentDirectory();
            File file = new File("");
            this.fileChooser.setSelectedFile(file);
            this.fileChooser.setCurrentDirectory(currentDirectory);
      } 
     else 
     {
            System.out.println("Attachment cancelled by user.");
     }
}//GEN-LAST:event_btnOpenMousePressed

    private void pnlForImageComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_pnlForImageComponentResized
        if(this.image != null)
        {
            this.image.setBounds(0, 0, this.pnlForImage.getWidth(), this.pnlForImage.getHeight());
            this.image.resizeImage(this.pnlForImage.getWidth(), this.pnlForImage.getHeight());
        }
    }//GEN-LAST:event_pnlForImageComponentResized

    private void combFiltreItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combFiltreItemStateChanged
       if(this.image != null)
       {
           this.image.setFiltre(this.combFiltre.getSelectedIndex());
           this.image.setTransition(this.combTransition.getSelectedIndex());
           this.image.displayImage();
       }
    }//GEN-LAST:event_combFiltreItemStateChanged

    private void lstImageValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstImageValueChanged
        try
        {
            int index;
            int lstSize;
            lstSize = this.imageListe.getSize();
            System.out.println("lstImageValueChanged - item remaning:"+lstSize);
            
            //on teste si la liste n'est pas vide ou si la liste n'a pas d'item de selectionner
            if( lstSize <= 0 && this.lstImage.isSelectionEmpty())
                return;
            
            index = this.lstImage.getSelectedIndex();            
            System.out.println("lstImageValueChanged - index:"+index);
            
            //on test si on n'a pas un index trop grand ou negatif
            if(index < 0 || lstSize-1 < index)
                return;
            this.pnlForImage.removeAll();
            this.image = this.imageListe.get(index).getPnlImage();
            this.pnlForImage.add(this.image);
            this.image.setFiltre(this.combFiltre.getSelectedIndex());
            this.image.setTransition(this.combTransition.getSelectedIndex());
            this.image.setBounds(0, 0, this.pnlForImage.getWidth(), this.pnlForImage.getHeight());
            this.image.displayImage();
            if(!image.isFadding())
            {
                image.setState(255);
                image.setFadding(true);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace(System.out);
            System.out.println("somthing went wrong in lstImageValueChanged");
        }
    }//GEN-LAST:event_lstImageValueChanged

    private void btnSaveMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMousePressed
        //si on a une image dans le buffer
        if(this.image != null && this.image.isInit())
        {
            this.fileChooser.setMultiSelectionEnabled(false);
            int returnVal = this.fileChooser.showSaveDialog(this);
            if(returnVal == JFileChooser.APPROVE_OPTION)
            {
                try
                {
                    File savedFile = this.fileChooser.getSelectedFile();
                    
                    File currentDirectory = this.fileChooser.getCurrentDirectory();
                    File file = new File("");
                    this.fileChooser.setSelectedFile(file);
                    this.fileChooser.setCurrentDirectory(currentDirectory);
                    
                    if(savedFile.exists())
                    {
                        Object[] options = {
                                            "Oui",
                                            "Non"
                                            };
                        String dialog = "le Fichier "+savedFile.getName()+"\n existe, voulez-vous l'ecraser?";
                        String title = "Le Fichier existe";
                        int n = JOptionPane.showOptionDialog(
                                        this,
                                        dialog,
                                        title,
                                        JOptionPane.YES_NO_OPTION,
                                        JOptionPane.QUESTION_MESSAGE,
                                        null,
                                        options,
                                        options[0]
                                    );
                        if(n != JOptionPane.YES_OPTION)
                        {
                            return;
                        }
                    }
                    BufferedImage img = this.image.getDisplayedImage();                                       
                    javax.imageio.ImageIO.write(img,"png",savedFile);
                    
                    this.imageListe.addElement(new ListItem(savedFile.getAbsolutePath()));
                    this.setSelectedDiapo(this.imageListe.getSize()-1);
                }
                catch(Exception e)
                {
                    System.out.println("Something went wrong during saving picture process");
                    e.printStackTrace(System.out);
                }
            }
        }
    }//GEN-LAST:event_btnSaveMousePressed

    private void btnDeleteMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMousePressed
        //first we check if we have selected something
        if(!this.lstImage.isSelectionEmpty())
        {
            int index = this.lstImage.getSelectedIndex();
            ListItem item = (ListItem) this.imageListe.elementAt(index);
            String fileName = item.getTitle();
            Object[] options = {
                                "Oui",
                                "Non"
                                };
            String dialog = "Etes-vous sur de vouloir supprimer le fichier "+fileName+"?\n"+
                            "Cette operation n'est pas reversible";
            String title = "Supprimer Le fichier";
            int n = JOptionPane.showOptionDialog(
                            this,
                            dialog,
                            title,
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.WARNING_MESSAGE,
                            null,
                            options,
                            options[0]
                        );
            //si on clique sur autre chose que oui
            if(n != JOptionPane.YES_OPTION)
            {
                return;
            }
            try
            {
                //on test si la liste sera vide
                if(this.imageListe.getSize() <= 1)
                {
                    System.out.println("DeleteImage imageListe vide");
                    this.image = new PnlImage();
                    this.pnlForImage.removeAll();
                    this.pnlForImage.updateUI();
                    this.imageListe.remove(index);
                    return;
                }
                System.out.println("Delete -> suppression d'une image");
                int newIndex = (index > (this.imageListe.getSize() - 1) ? (this.imageListe.getSize() - 1 ): index);
                
                this.setSelectedDiapo(newIndex);
                
                String path = item.getImgPath();
                File file = new File(path);
                file.delete();
                this.imageListe.remove(index);
            }
            catch(Exception e)
            {
                System.out.println("Erreur lors de la suppression du fichier "+fileName);
                e.printStackTrace(System.out);
            }
        }
    }//GEN-LAST:event_btnDeleteMousePressed

    private void btnSupprDiapoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSupprDiapoMousePressed
        if(!this.lstImage.isSelectionEmpty())
        {
            int index = this.lstImage.getSelectedIndex(); 
            //on test si la liste sera vide
            if(this.imageListe.getSize() <= 1)
            {
                System.out.println("Suppression image diapo, imageListe vide");
                this.image = new PnlImage();
                this.pnlForImage.removeAll();
                    this.pnlForImage.updateUI();
                this.imageListe.remove(index);
                //si on était en train de faire défiler les diapo et que la liste est vide
                if(!this.isPaused)
                {
                    this.isPaused = true;
                    this.changePlayState("pause");
                }
                return;
            }
            System.out.println("Suppression image diapo, imageListe suppression d'une diapo");
            int newIndex = ( (index + 1) > (this.imageListe.getSize() - 1) ? (this.imageListe.getSize() - 1 ): index + 1);
            this.imageListe.remove(index);
            this.setSelectedDiapo(newIndex - 1);
        }
    }//GEN-LAST:event_btnSupprDiapoMousePressed

    private void btnUpDiapoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpDiapoMousePressed
        if(!this.lstImage.isSelectionEmpty())
        {
            int index = this.lstImage.getSelectedIndex();
            if(index <= 0)
                return;
            ListItem item = this.imageListe.get(index-1);
            this.imageListe.set(index-1, this.imageListe.get(index));
            this.imageListe.set(index, item);
            this.setSelectedDiapo(index-1);
        }
    }//GEN-LAST:event_btnUpDiapoMousePressed

    private void btnDownDiapoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDownDiapoMousePressed
        if(!this.lstImage.isSelectionEmpty())
        {
            int index = this.lstImage.getSelectedIndex();
            if(index >= this.imageListe.size() - 1)
                return;
            ListItem item = this.imageListe.get(index+1);
            this.imageListe.set(index+1, this.imageListe.get(index));
            this.imageListe.set(index, item);
            this.setSelectedDiapo(index+1);
        }
    }//GEN-LAST:event_btnDownDiapoMousePressed

    private void btnNextMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNextMousePressed
        this.changeDisplayedImage(3);
    }//GEN-LAST:event_btnNextMousePressed

    private void btnRetStepMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRetStepMousePressed
        this.changeDisplayedImage(2);
    }//GEN-LAST:event_btnRetStepMousePressed

    private void btnStartMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStartMousePressed
        this.changeDisplayedImage(1);
    }//GEN-LAST:event_btnStartMousePressed

    private void btnEndMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEndMousePressed
        this.changeDisplayedImage(4);
    }//GEN-LAST:event_btnEndMousePressed

    private void btnPauseMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPauseMousePressed
        this.isPaused = true;
        this.changePlayState("pause");
    }//GEN-LAST:event_btnPauseMousePressed

    private void btnPlayMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPlayMousePressed
        System.out.println("Play selection empty? "+this.lstImage.isSelectionEmpty());
        //if nothing is selected
        if(this.lstImage.isSelectionEmpty())
            this.changeDisplayedImage(3);
                                            
        System.out.print("Play selected index: "+this.lstImage.getSelectedIndex()+ " ");
        System.out.println("Liste Size:" + (this.imageListe.getSize()-1) + " ");
        System.out.println("Loop is Selected? "+this.tgBtnLoop.isSelected());
        //if is last image on list and loop is not selected, we don't play
        if(this.lstImage.getSelectedIndex() >= this.imageListe.getSize()-1 && !this.tgBtnLoop.isSelected())
            return;
        
        this.changePlayState("play");
        this.isPaused = false;
        this.time.start();
    }//GEN-LAST:event_btnPlayMousePressed

    private void btnStopMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStopMousePressed
        this.isPaused = true;
        this.time.stop();
        this.pbTimeProgress.setValue(0);
        this.changePlayState("stop");
        this.changeDisplayedImage(1);
    }//GEN-LAST:event_btnStopMousePressed

    private void lstImageMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstImageMousePressed
        this.pbTimeProgress.setValue(0);
    }//GEN-LAST:event_lstImageMousePressed

    private void combTransitionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combTransitionItemStateChanged
       if(this.image != null)
       {
           this.image.setFiltre(this.combFiltre.getSelectedIndex());
           this.image.setTransition(this.combTransition.getSelectedIndex());
           this.image.displayImage();
       }
    }//GEN-LAST:event_combTransitionItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDownDiapo;
    private javax.swing.JButton btnEnd;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnOpen;
    private javax.swing.JButton btnPause;
    private javax.swing.JButton btnPlay;
    private javax.swing.JButton btnRetStep;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnStart;
    private javax.swing.JButton btnStop;
    private javax.swing.JButton btnSupprDiapo;
    private javax.swing.JButton btnUpDiapo;
    private javax.swing.JComboBox combFiltre;
    private javax.swing.JComboBox combTransition;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblForFiltre;
    private javax.swing.JLabel lblForProgess;
    private javax.swing.JLabel lblForVitesse;
    private javax.swing.JLabel lblTimeState;
    private javax.swing.JLabel lblVitesse;
    private javax.swing.JList lstImage;
    private javax.swing.JProgressBar pbTimeProgress;
    private javax.swing.JPanel pnlDiapoNav;
    private javax.swing.JPanel pnlDiapoSpeed;
    private javax.swing.JPanel pnlDiapositive;
    private javax.swing.JPanel pnlFile;
    private javax.swing.JPanel pnlFileDisp;
    private javax.swing.JPanel pnlForImage;
    private javax.swing.JPanel pnlImage;
    private javax.swing.JPanel pnlNavigation;
    private javax.swing.JScrollPane scrlListeImage;
    private javax.swing.JSlider sldVitesse;
    private javax.swing.JToggleButton tgBtnLoop;
    private javax.swing.JToggleButton tgBtnRandom;
    // End of variables declaration//GEN-END:variables
}
