/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listItem;

import java.awt.Color;
import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author le_forro
 */
public class ItemCellRender  extends JLabel implements ListCellRenderer
{
  private static final Color HIGHLIGHT_COLOR = new Color(150, 190, 250);
  private static final Color OVER_COLOR = new Color(204, 204, 204);
  private static final Color DEFAULT_COLOR = new Color(250, 250, 250);
  private ImageIcon imageIcone;
  private final int iconSize;
  
  public ItemCellRender()
  {
    this.setOpaque(true);
    this.setIconTextGap(12);
    this.iconSize = 50;
    this.setVerticalAlignment(CENTER);
    this.setHorizontalAlignment(LEFT);
    this.setBackground(DEFAULT_COLOR);
    this.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED,DEFAULT_COLOR,DEFAULT_COLOR));
  }
    
  @Override
  public Component getListCellRendererComponent(
          JList list, Object value, int index, boolean isSelected, 
          boolean cellHasFocus ) 
  {
      System.out.println("ItemCellRender getListCellRendererComponent() index:"+index+" selected:"+isSelected);
      ListItem item = (ListItem) value;
      this.setText(item.getTitle());
      if(null == this.imageIcone)
          this.imageIcone = item.getImage(this.iconSize);
      this.setIcon(item.getImage(this.iconSize));
      if (isSelected || list.getSelectedIndex() == index) 
      {
          this.setBackground(HIGHLIGHT_COLOR);
          this.setForeground((cellHasFocus ? Color.cyan :Color.white));
      } 
      else
        if (cellHasFocus) 
        {
            this.setBackground(OVER_COLOR);
            this.setForeground(Color.white);
        } 
        else 
        {
            this.setBackground(DEFAULT_COLOR);
            this.setForeground(Color.black);
        }
      return this;
  }
}
