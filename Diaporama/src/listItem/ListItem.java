/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listItem;

import java.io.File;
import pnlImage.PnlImage;
import javax.swing.ImageIcon;

/**
 *
 * @author le_forro
 */
public class ListItem
{
  private final String fileTitle;

  private final String imagePath;

  private final PnlImage pnlImage;
  
  private ImageIcon imageIcone;
  
  private int imageIconeSize;
  
  public ListItem( String imagePath)
  {
    this.imagePath = imagePath;
    this.pnlImage = new PnlImage(imagePath);
    this.fileTitle = ListItem.getFileName(imagePath);
  }
  public ListItem(String title, String imagePath)
  {
    this.imagePath = imagePath;
    this.pnlImage = new PnlImage(imagePath);
    this.fileTitle = title;
  }
  
  private static final String getFileName(String path)
  {
    File f = new File(path);
    return f.getName();
  }
  
  public String getTitle() 
  {
      System.out.println("listItem getTitle()");
    return this.fileTitle;
  }

  public PnlImage getPnlImage()
  {
      System.out.println("listItem getPnlImage()");
      return this.pnlImage;
  }
  
  public String getImgPath() 
  {
      System.out.println("listItem getImgPath()");
    return this.imagePath;
  }
  public ImageIcon getImage()
  {
      int size = 50;
      System.out.println("listItem getImage()");
      if(this.imageIconeSize != size)
      {
          this.imageIconeSize = size;
          this.imageIcone = this.pnlImage.getImageAsIcon(size);
      }
      else
          if(this.imageIcone == null)
             this.imageIcone = this.pnlImage.getImageAsIcon(size);
      return this.imageIcone;
  }
  public ImageIcon getImage(int size)
  {
      System.out.println("listItem getImage(size)");
      if(this.imageIconeSize != size)
      {
          this.imageIconeSize = size;
          this.imageIcone = this.pnlImage.getImageAsIcon(size);
      }
      else
          if(this.imageIcone == null)
             this.imageIcone = this.pnlImage.getImageAsIcon(size);
      return this.imageIcone;
  }

  // Override standard toString method to give a useful result
  @Override
  public String toString()
  {
    return this.getTitle();
 
  }
}