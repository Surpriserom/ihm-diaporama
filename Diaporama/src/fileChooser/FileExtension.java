/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileChooser;

import java.awt.Image;
import java.io.File;
import java.lang.reflect.Field;
import javax.swing.ImageIcon;

/**
 *
 * @author romain
 */
public class FileExtension 
{
    public final static String jpeg = "jpeg";
    public final static String jpg = "jpg";
    public final static String gif = "gif";
    public final static String png = "png";
    
    public FileExtension()
    {
    }
    
    /**
     * Get the extension of a file in lowercase.
     * @param f a file
     * @return String the file extension
     */
    public static String getExtension(File f)
    {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1)
        {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
    
    /** 
     * Returns an ImageIcon, or null if the path was invalid.
     * @param path image path
     * @return ImageIcon | null
     */
    protected static ImageIcon createImageIcon(String path) 
    {
        java.net.URL imgURL = FileExtension.class.getResource(path);
        if (imgURL != null)
        {
            return new ImageIcon(imgURL);
        } 
        else 
        {
            System.err.println("Le fichier " + path + " n'a pas ete trouve.");
            return null;
        }
    }
    /** 
     * Returns an ImageIcon, or null if the path was invalid.
     * @param path image path
     * @param size image icon size
     * @return ImageIcon | null
     */
    protected static ImageIcon createImageIcon(String path, int size) 
    {
        java.net.URL imgURL = FileExtension.class.getResource(path);
        if (imgURL != null)
        {
            try
            {
                Image img = javax.imageio.ImageIO.read(imgURL);
                img = img.getScaledInstance(size, size,size);
                return new ImageIcon(img);
            }
            catch( Exception e)
            {
                System.out.println("ImageIcon, something wen't wrong :/");
                e.printStackTrace(System.out);
            }
            return null;
        } 
        else 
        {
            System.err.println("Le fichier " + path + " n'a pas ete trouve.");
            return null;
        }
    }
    
    /**
     * Return true if the file extension is part of the autorised file in class
     * @param extension the file extension we want to check
     * @return boolean
     */
    public boolean inAutorisedFileList(String extension)
    {
        try
        {
            Class<?> objClass = this.getClass();
            Field[] fields = objClass.getFields();
            for(Field field : fields) 
            {
                String value = (String) field.get(this);
                if(value.equals(extension))
                    return true;
            }
        }
        catch(Exception e)
        {
            System.out.println("Echec comparaison liste des extension");
            e.printStackTrace(System.out);
        }
        return false;
    }
    
    /**
     * Return true if the file extension is part of the autorised file in class && in autorisers array
     * @param extension the file extension we want to check
     * @param autorisers the array of file we want the extension to be
     * @return boolean
     */
    public boolean inAutorisedFileList(String extension, String[] autorisers)
    {
        try
        {
            Class<?> objClass = this.getClass();
            Field[] fields = objClass.getFields();
            for(Field field : fields) 
            {
                String value = (String) field.get(this);
                if(value.equals(extension))
                    for(String autoriser: autorisers)
                        if(autoriser.equals(value))
                            return true;
            }
        }
        catch(Exception e)
        {
            System.out.println("Echec comparaison liste des extension");
            e.printStackTrace(System.out);
        }
        return false;
    }
}
