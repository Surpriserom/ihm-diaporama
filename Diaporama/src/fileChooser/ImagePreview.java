/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileChooser;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;

/**
 *
 * @author romain
 */
public class ImagePreview extends JComponent implements PropertyChangeListener
{
    private ImageIcon thumbnail;
    private File file;

    public ImagePreview(JFileChooser fc)
    {
        this.setPreferredSize(new Dimension(200, 100));
        fc.addPropertyChangeListener(this);
    }
    
    public void loadImage()
    {
        if (file == null)
        {
            this.thumbnail = null;
            return;
        }

        ImageIcon tmpIcon = new ImageIcon(file.getPath());
        //si l'icone est trop large on redimensionne en largeur
        if (tmpIcon.getIconWidth() > 190)
            tmpIcon = new ImageIcon(tmpIcon.getImage().
                                          getScaledInstance(190, -1,
                                                      Image.SCALE_DEFAULT));
        if(tmpIcon.getIconHeight() > 90)
            tmpIcon = new ImageIcon(tmpIcon.getImage().
                                          getScaledInstance(-1, 90,
                                                      Image.SCALE_DEFAULT));
        this.thumbnail = tmpIcon;
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent e)
    {
        boolean update;
        String prop = e.getPropertyName();

        switch(prop)
        {
            //If the directory changed, don't show an image.
            case JFileChooser.DIRECTORY_CHANGED_PROPERTY:
                                                    file = null;
                                                    update = true;
                                                break;
            //If a file became selected, find out which one.
            case JFileChooser.SELECTED_FILE_CHANGED_PROPERTY:
                                                    file = (File) e.getNewValue();
                                                    update = true;
                                                break;
            //if none of above don't update
            default : update = false;
                    break;
        }

        //Update the preview accordingly.
        if (update)
        {
            this.thumbnail = null;
            if (isShowing())
            {
                loadImage();
                repaint();
            }
        }
    }
    
    @Override
    protected void paintComponent(Graphics g)
    {
        if (this.thumbnail == null)
        {
            loadImage();
            if(this.thumbnail == null)
                return;
        }
        
        
        int x = getWidth()/2 - this.thumbnail.getIconWidth()/2;
        int y = getHeight()/2 - this.thumbnail.getIconHeight()/2;

       if (y < 0)
       {
           y = 0;
       }
       if (x < 5){
           x = 5;
       }
       this.thumbnail.paintIcon(this, g, x, y);
    }
}
