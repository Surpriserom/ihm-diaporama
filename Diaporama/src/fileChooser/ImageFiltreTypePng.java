/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileChooser;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author romain
 */
public class ImageFiltreTypePng  extends FileFilter
{
    private FileExtension fileExten;
    private final String[] fileType = {"png"};
    
    @Override
    public boolean accept(File file) 
    {
        if (file.isDirectory())
        {
            return true;
        }
        if(this.fileExten == null)
            this.fileExten = new FileExtension();
            
        String extension = FileExtension.getExtension(file);
        if (
                (extension != null)
            && 
                (
                    this.fileExten.inAutorisedFileList(extension, this.fileType)
                )
            )
            {
                    return true;
            } 
        else 
            return false;
    }
    
    @Override
    public String getDescription()
    {
        return "Images png";
    }
}
