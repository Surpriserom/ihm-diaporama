/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileChooser;

import java.io.File;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.filechooser.FileView;

/**
 *
 * @author romain
 */
public class ImageFileView  extends FileView 
{
    ImageIcon jpgIcon = FileExtension.createImageIcon("FileIcon/jpg.png");
    ImageIcon gifIcon = FileExtension.createImageIcon("FileIcon/gif.png");
    ImageIcon pngIcon = FileExtension.createImageIcon("FileIcon/png.png");
    ImageIcon folderIcon = FileExtension.createImageIcon("FileIcon/folder.png", 16);
    
    @Override
    public String getName(File f)
    {
        String fname = f.getName();
        if(fname == null || fname.equals(""))
            fname = f.getPath();
        return fname;
    }

    @Override
    public String getDescription(File f)
    {
        return null;
    }

    @Override
    public Boolean isTraversable(File f)
    {
        return null;
    }

    @Override
    public String getTypeDescription(File f)
    {
        String extension = FileExtension.getExtension(f);
        String type;

        switch(extension)
        {
            case FileExtension.jpeg :
            case FileExtension.jpg:
                            type = "JPEG Image";
                   break;
            case FileExtension.gif:
                            type = "GIF Image";
                    break;
            case FileExtension.png:
                            type = "PNG Image";
                    break;
            default: type = null;
        }
        return type;
    }

    @Override
    public Icon getIcon(File f)
    {
        if(f.isDirectory())
        {
            return this.folderIcon;
        }
        
        String extension = FileExtension.getExtension(f);
        Icon icon;
        if(extension == null)
            return null;
            
        switch(extension)
        {
            case FileExtension.jpeg :
            case FileExtension.jpg:
                            icon = this.jpgIcon;
                   break;
            case FileExtension.gif:
                            icon = this.gifIcon;
                    break;
            case FileExtension.png:
                            icon = this.pngIcon;
                    break;
            default : icon = null;
        }
        return icon;
    }
}
