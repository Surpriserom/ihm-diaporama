package pnlImage;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 * @author romain
 */
public class ImageTransition 
{
    public ImageTransition()
    {
        
    }
    public static BufferedImage setTransition(int transition, int width, int height)
    {
        BufferedImage img = null;
        switch(transition)
        {
            case 1 :
                  img = ImageTransition.WhiteTransition(width, height);
                break;
            case 2 :
                  img = ImageTransition.WhiteTransition(width, height);
                break;
            default:
                break;
        }
        return img;
    }
    
    public static BufferedImage WhiteTransition(int width, int height)
    {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        for(int x=0; x<img.getWidth(); x++)
         for(int y=0; y<img.getHeight(); y++)
         {
            int red = 255;
            int green = 255;
            int blue = 255;
            int alpha = 255;
            
            if(alpha < 0) alpha = 0;
            else if(alpha > 255) alpha = 255;
            
            Color color = new Color(red,green,blue,alpha);
            img.setRGB(x, y, color.getRGB());
         }
        return img;
    }
    
    public static BufferedImage upDateAlpha(int width, int height, int alphaValue, BufferedImage img)
    {
        for(int x=0; x<img.getWidth(); x++)
         for(int y=0; y<img.getHeight(); y++)
         {
             int pixel = img.getRGB(x, y);
            int red = (pixel >> 16) & 0xff;
            int green = (pixel >> 8) & 0xff;
            int blue = (pixel) & 0xff;
            int alpha = alphaValue;
            
            if(alpha < 0) alpha = 0;
            else if(alpha > 255) alpha = 255;
            
            Color color = new Color(red,green,blue,alpha);
            img.setRGB(x, y, color.getRGB());
         }
        return img;
    }
}
