/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * PnlImage.java
 *
 * Created on 14 oct. 2014, 17:33:34
 */
package pnlImage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.Timer;

/**
 *
 * @author le_forro
 */
public class PnlImage extends javax.swing.JPanel
{

    private BufferedImage imgOriginal = null;
    private BufferedImage imgConverted = null;
    private String imgPath;
    private int filtre;
    private JLabel lblImage;
    
    private Timer time;
    private ActionListener timerListener;
    
    private int transition;
    private int state;
    private Boolean faddingIn;
    private Boolean fadding;
    private BufferedImage imgTransition;
    private JLabel lblTransition;
    
    //create
    /**
     * Creates new form PnlImage
     */
    public PnlImage() {
        initComponents();
        this.filtre = 0;
        this.transition = 0;
        this.state = 255;
        this.fadding = false;
        this.faddingIn = true;
        this.initTimer();
    }
    /**
     * Creates new form PnlImage with the image at path
     * @param path path for an image
     */
    public PnlImage(String path) {
        initComponents();
        this.imgPath = path;
        this.filtre = 0;
        this.transition = 0;
        this.state = 255;
        this.fadding = false;
        this.faddingIn = true;
        this.initTimer();
    }
     private void initTimer()
    {
        this.timerListener = new ActionListener() 
                                {

                                    @Override
                                    public void actionPerformed(ActionEvent e) 
                                    {
                                        updateTransition(state);
                                        int step = 10;
                                        if(transition == 1)
                                            step = 51;
                                        if(fadding)
                                        {
                                            if(faddingIn)
                                                state-=step;
                                            else
                                                state+=step;
                                        }
                                        if(state >= 255)
                                        {
                                            setFadding(false);
                                            faddingIn = true;
                                            state = 255;
                                        }
                                        if(state <= 0)
                                        {
                                            setFadding(false);
                                            faddingIn = false;
                                            state = 0;
                                        }
                                    }
                                };
        this.time = new Timer(10, this.timerListener);
    }
    public void updateTransition(int val)
    {
        if(this.imgTransition == null)
        {
            ImageTransition.setTransition(this.transition, this.getWidth(), this.getHeight());
        }
        if(this.transition == 1)
        {
           ImageTransition.upDateAlpha(this.getWidth(), this.getHeight(), val, this.imgTransition);
           this.lblTransition.repaint();
        }
    }
    
    //setter
    /**
     * set path to image
     * @param path 
     */
    public void setPath(String path)
    {
        System.out.println("set path");
        this.imgPath = path;
    }
    
    /**
     * set the filter it gonna be used to display the image
     * @param filtre 
     */
    public void setFiltre(int filtre)
    {
        System.out.println("set filtre "+Integer.toString(filtre));
        this.filtre = filtre;
    }
    
    /**
     * set the transition to apply to the image
     * @param transition 
     */
    public void setTransition(int transition)
    {
        System.out.println("set transition "+Integer.toString(transition));
        this.transition = transition;
        if(this.getWidth()>0 && this.getHeight() > 0)
            this.imgTransition = ImageTransition.setTransition(transition, this.getWidth(),this.getHeight());
    }
    
    /**
     * set if we need to display a transition effect
     * @param val 
     */
    public void setFadding(Boolean val)
    {
        System.out.println("fadding is "+val);
        this.fadding = val;
        if(val)
        {
            this.time.start();
            if(this.lblTransition != null)
                this.lblTransition.setVisible(true);
        }
        else
        {
            this.time.stop();
            if(this.lblTransition != null)
                this.lblTransition.setVisible(false);
        }
    }
    
    /**
     * set the fadding state 255 opaque 0 transparent
     * @param val 
     */
    public void setState(int val)
    {
        //si on a 255 pour l'etat on affiche l'uimage on fait un fadd in effect
        if(val == 255)
            this.faddingIn = true;
        //si on a 0 on fait un fade out effect
        if(val == 0)
            this.faddingIn = false;
        this.state = val;
    }
    
    /**
     * make the label image with the image converted
     */
    private void setLblImage()
    {
        System.out.println("set label image");
        this.lblImage = new JLabel(new ImageIcon(this.imgConverted));
    }
    /**
     *  make the label image with the image converted, 
     * image size are set with given parameter
     * @param width
     * @param height 
     */
    private void setLblImage( int width, int height)
    {
        System.out.println("set label image with size");
        //on redimensionne en conservan les proportion pour se faire on tien compte de la plus petit dimension dans l'espace disponible
        this.lblImage = new JLabel(new ImageIcon(ImageResize.resize(this.imgConverted,height , width)));
    }
    
    /**
     * init the lalbel we gonna display transition effect in
     * @param witdh
     * @param height 
     */
    private void setLblTransition(int witdh, int height)
    {
        System.out.println("set transition with size h:"+height+" w:"+witdh);
        if(this.imgTransition == null)
            this.imgTransition = ImageTransition.setTransition(transition, witdh, height);
        if(this.imgTransition.getHeight() != height || this.imgTransition.getWidth() != witdh)
            this.imgTransition = ImageTransition.setTransition(transition, witdh, height);
        System.out.println("imgTransition is null :"+(this.imgTransition == null));
        this.lblTransition = new JLabel(new ImageIcon(this.imgTransition));
        this.lblTransition.setOpaque(false);
        this.lblTransition.setVisible(false);
    }
    
    /**
     * init the lalbel we gonna display transition effect in
     */
    private void setLblTransition()
    {
        this.setLblTransition(this.getWidth(), this.getHeight());
    }
    
    //getter
    /**
     * get the image that is displayed at the size 
     * it is displayed and with given filter
     * @return converted image
     */
    public BufferedImage getDisplayedImage()
    {
        return ImageResize.resize(this.imgConverted,this.getHeight() , this.getWidth());
    }
    
    /**
     * return an icone of the image
     * @param size the size of the icone we want
     * @return ImageIcon
     */
    public ImageIcon getImageAsIcon(int size)
    {
        if(this.imgOriginal == null)
           this.openImageFile();
       return new ImageIcon(ImageResize.resize(this.imgOriginal,size));
    }
    
    public Boolean isFadding()
    {
        return this.fadding;
    }
    
    //test
    
    /**
     * return true if image converted is not null
     * @return boolean
     */
    public boolean isInit()
    {
        return (this.imgConverted != null);
    }
    
    //utility
    /**
     * open image file with the path
     */
    private void openImageFile()
    {
        System.out.println("open image");
        try
        {
            this.imgOriginal = ImageIO.read(new File(this.imgPath));
        }
        catch(Exception e)
        {
            System.out.println("Error loading image");
            e.printStackTrace(System.out);
        }
    }
    /**
     * apply the selected filter default is original image
     * 1 for black and white
     * 2 for sepia
     * 3 for negative effect
     */
    private void convertImage()
    {
        System.out.println("convert image");
        switch(this.filtre)
        {
            case 1 ://echelle de gris
                      this.imgConverted = ImageFiltre.toGrayScale(this.imgOriginal); 
                    break;
            case 2 ://sepia
                      this.imgConverted = ImageFiltre.toSepia(this.imgOriginal, 4); 
                    break;
            case 3 ://negatif
                      this.imgConverted = ImageFiltre.toNegate(this.imgOriginal); 
                    break;
            case 0:// filtre 0, image original
            default:
                    this.imgConverted = this.imgOriginal;
                   break;
        }
    }
    
    /**
     * display the image bound to panel size
     */
    public void displayImage()
    {
        System.out.println("display image");
        //before displaying pic, we clean all already here
        this.removeAll();
        if(this.imgOriginal == null)
           this.openImageFile();
        this.convertImage();
        this.setLblImage(this.getWidth(), this.getHeight());
        this.add(this.lblImage);
        this.lblImage.setBounds(0, 0, this.getWidth(), this.getHeight());
        if(this.transition != 0)
        {
            this.setLblTransition();
            this.lblImage.add(this.lblTransition);
            this.lblTransition.setBounds(0, 0, this.getWidth(), this.getHeight());
        }
        
    }
    
    /**
     * change image size according to panel size
     */
    public void resizeImage()
    {
        int width, height;
        width = this.getWidth();
        height = this.getHeight();
        System.out.println("resize image");
        this.removeAll();
        this.setLblImage(width, height);
        this.add(this.lblImage);
        this.lblImage.setBounds(0, 0, width, height);
        this.setLblTransition();
        this.add(this.lblTransition);
        this.lblTransition.setBounds(0, 0, this.getWidth(), this.getHeight());
    }
    
    /**
     * change image size according given parameter
     * @param width
     * @param height 
     */
    public void resizeImage(int width, int height)
    {
        System.out.println("resize image with width / height");
        this.removeAll();
        this.setLblImage(width, height);
        this.add(this.lblImage);
        this.lblImage.setBounds(0, 0, width, height);
        this.setLblTransition();
        this.add(this.lblTransition);
        this.lblTransition.setBounds(0, 0, this.getWidth(), this.getHeight());
    }
    
    public void moveImage(int x, int y)
    {
        System.out.println("move image at x:"+x+" y:"+y);
        this.lblImage.setBounds(x, y, this.getWidth(), this.getHeight());
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
