/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pnlImage;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

/**
 *
 * @author romain
 */
public class ImageResize
{
    /**
     * fonction pour redimensionner une image par rapport a une taille
     * @param image BufferedImage a redimensionner
     * @param size 
     * @return BufferedImage image redimensionner
     */
    public static BufferedImage resize(BufferedImage image, int size)
    {
        System.out.println("resize buffered image");
        int height, width;
        float h = image.getHeight();
        float w = image.getWidth();
        //on redimensionne par rapport a taille, on conserve la plus grande dimension a l'echele fournit, 
        //on redimensionne l'autre en conservant le rapport de dimension
        if(h>w)
        {
            width = Math.round(size * (w/h));
            height = size;
        }
        else
        {
            height =  Math.round(size * (h/w));
            width = size;
        }               
        System.out.println("height: "+height+" | width: "+width);
        
        //BufferedImage.TRANSLUCENT <= pour une image quo peut contenir une valeur alpha (transparence) .png
        BufferedImage resizedImg = new BufferedImage( width, height, BufferedImage.TRANSLUCENT);
        //on utilise la librarie Graphics2D pour redessiner l'image a la dimension shouaiter
        Graphics2D g2d = resizedImg.createGraphics();
        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
        g2d.drawImage(image, 0, 0, width, height, null);
        g2d.dispose();
        return resizedImg;
    }
    
    /**
     * fonction pour redimensionner une image par rapport au contenant
     * @param image buffered image a redimensionner
     * @param height hauteur de l'image a atteindre
     * @param width largeur de l'image a atteindre
     * @return BufferedImage image redimensionner
     */
    public static BufferedImage resize(BufferedImage image, int height, int width)
    {
        System.out.println("resize buffered image with param");
        float imH = (float) image.getHeight();
        float imW = (float) image.getWidth();
        int newHeight;
        int newWidth;
        float newSize;
        System.out.println("height: "+height+" | width: "+width);
         if(width > height)
        {
            newSize = imW / imH * height;
            newHeight = height;
            newWidth = Math.round(newSize);
            
            if(newWidth > width)
            {
                newSize = (float) newHeight / (float) newWidth * width;
                newHeight = Math.round(newSize);
                newWidth = width;
            }
        }
        else
        {
            newSize = imH / imW * width;
            newHeight = Math.round(newSize);
            newWidth = width;
            
            if(newHeight > height)
            {
                newSize = (float) newWidth / (float) newHeight * height;
                newHeight = height;
                newWidth = Math.round(newSize);
            }
        }
        
        System.out.println("height: "+newHeight+" | width: "+newWidth);
        
        //BufferedImage.TRANSLUCENT <= pour une image qui peut contenir une valeur alpha (transparence) .png
        BufferedImage resizedImg = new BufferedImage( newWidth, newHeight, BufferedImage.TRANSLUCENT);
        //on utilise la librarie Graphics2D pour redessiner l'image a la dimension shouaiter
        Graphics2D g2d = resizedImg.createGraphics();
        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
        g2d.drawImage(image, 0, 0, newWidth, newHeight, null);
        g2d.dispose();
        return resizedImg;
    }
}
